import boto3

from logsize.constants import END_DATE, REGION, START_DATE
from logsize.logsize import (
    generate_log_groups_report,
    get_log_group_stats,
    get_log_groups_info,
)


def main():

    session = boto3.Session(profile_name="saml", region_name=REGION)

    cloudwatch_client = session.client("cloudwatch")
    logs_client = session.client("logs")

    print(f"[INFO] Gathering log group information in {REGION}")
    log_groups_info = get_log_groups_info(logs_client)

    print("[INFO] Querying cloudwatch metrics for all log groups in the account")
    log_groups_stats = get_log_group_stats(
        log_groups_info, cloudwatch_client, START_DATE, END_DATE
    )

    print(
        f"[INFO] Got results for {len(log_groups_stats)} out of {len(log_groups_info)} log groups."
    )

    print("[INFO] Saving results to csv file.")
    generate_log_groups_report(log_groups_stats)


if __name__ == "__main__":
    main()
