import csv

from logsize.helpers import convert_size


def get_log_groups_info(logs_client) -> list:
    """
    Retrieves information about log groups using an AWS CloudWatch Logs client.

    Args:
        logs_client: An instance of the AWS CloudWatch Logs client.

    Returns:
        list: A list of dictionaries containing information about log groups.

    Raises:
        botocore.exceptions.ClientError: If an error occurs while communicating with the AWS service.

    Example:
        >>> import boto3
        >>> logs_client = boto3.client("logs", region_name="us-east-1")
        >>> log_groups_info = get_log_groups_info(logs_client)
        >>> log_groups_info[:2]
        [{'logGroupName': '/aws/lambda/my-lambda-function', 'creationTime': 1639492800000, 'metricFilterCount': 0, 'arn': 'arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/my-lambda-function:*', 'storedBytes': 0, 'retentionInDays': 90}, {'logGroupName': '/aws/lambda/another-lambda-function', 'creationTime': 1639415640000, 'metricFilterCount': 0, 'arn': 'arn:aws:logs:us-east-1:123456789012:log-group:/aws/lambda/another-lambda-function:*', 'storedBytes': 0, 'retentionInDays': 90}]
    """
    logs_paginator = logs_client.get_paginator("describe_log_groups")
    logs_result = logs_paginator.paginate().build_full_result()
    return logs_result["logGroups"]


def get_log_group_stats(
    log_groups: list, cloudwatch_client, start_date: str, end_date: str
) -> list:
    """
    Retrieves statistics for log groups within a specified time range using AWS CloudWatch Metrics.

    Args:
        log_groups (list): A list of dictionaries representing log groups.
            Each dictionary should contain at least the key "logGroupName" with the name of the log group.
        cloudwatch_client: An instance of the AWS CloudWatch client.
        start_date (datetime): The start date of the time range for which statistics are to be retrieved.
        end_date (datetime): The end date of the time range for which statistics are to be retrieved.

    Returns:
        list: A list of tuples containing statistics for each log group. Each tuple includes:
            - Log Group Name (str): The name of the log group.
            - Total Incoming Bytes (str): The total incoming bytes in the specified time range.
            - Average Incoming Bytes (str): The average incoming bytes per period in the specified time range.

    Raises:
        botocore.exceptions.ClientError: If an error occurs while communicating with the AWS service.
        ValueError: If `start_date` is after `end_date`.

    Example:
        >>> import boto3
        >>> from datetime import datetime, timedelta
        >>> cloudwatch_client = boto3.client("cloudwatch", region_name="us-east-1")
        >>> log_groups = [{"logGroupName": "/aws/lambda/my-lambda-function"}]
        >>> start_date = datetime.now() - timedelta(days=30)
        >>> end_date = datetime.now()
        >>> log_group_stats = get_log_group_stats(log_groups, cloudwatch_client, start_date, end_date)
        >>> log_group_stats[:2]
        [('log_group_name_1', '2.0 KB', '1.0 KB'), ('log_group_name_2', '1.5 KB', '0.75 KB')]
    """
    log_stats = []

    for group in log_groups:
        log_group_name = group["logGroupName"]
        stats = cloudwatch_client.get_metric_statistics(
            Namespace="AWS/Logs",
            MetricName="IncomingBytes",
            StartTime=start_date,
            EndTime=end_date,
            Period=3600 * 24 * 30,
            Statistics=["Sum", "Average"],
            Unit="Bytes",
            Dimensions=[{"Name": "LogGroupName", "Value": log_group_name}],
        )

        if len(stats.get("Datapoints")):
            stats_data = stats.get("Datapoints")[0]
            stats_sum = convert_size(round(stats_data.get("Sum"), 2))
            stats_avg = convert_size(round(stats_data.get("Average"), 2))

            log_stats.append([log_group_name, stats_sum, stats_avg])

    return log_stats


def generate_log_groups_report(
    log_stats: list, filename: str = "log_group_ingestion"
) -> None:
    """
    Generates a CSV report containing statistics of log groups.

    Args:
        log_stats (list): A list of tuples containing log group statistics. Each tuple should have three elements:
            - Log Group Name (str): The name of the log group.
            - Total Ingestion (30 Days) (str or int): The total ingestion in the last 30 days.
            - Average Ingestion (30 Days) (str or float): The average ingestion per day in the last 30 days.
        filename (str, optional): The filename of the generated CSV report. Defaults to "log_group_ingestion".

    Returns:
        None

    Raises:
        TypeError: If `log_stats` is not a list or if any tuple in `log_stats` does not contain three elements.
        ValueError: If `filename` is an empty string.

    Example:
        >>> log_stats = [("Group A", 500, 16.67), ("Group B", 1000, 33.33)]
        >>> generate_log_groups_report(log_stats, "log_stats_report")
        # This will generate a CSV file named "log_stats_report.csv" containing the log group statistics.
    """

    for stat in log_stats:
        if not isinstance(stat, list) or len(stat) != 3:
            raise TypeError(
                "Each element in log_stats must be a tuple with three elements."
            )

    if not isinstance(log_stats, list):
        raise TypeError("log_stats must be a list.")

    if filename == "":
        raise ValueError("Filename cannot be empty.")

    with open(f"{filename}.csv", "w", newline="") as file:
        writer = csv.writer(file)
        columns = [
            "Log Group Name",
            "Total Ingestion (30 Days)",
            "Average Ingestion (30 Days)",
        ]
        writer.writerow(columns)

        for stat in log_stats:
            writer.writerow(stat)
