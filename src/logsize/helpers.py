import math


def convert_size(size_bytes: int) -> str:
    """
    Converts a size in bytes to a human-readable format.

    Args:
        size_bytes (int): The size in bytes to be converted.

    Returns:
        str: A human-readable string representing the converted size.

    Raises:
        ValueError: If `size_bytes` is negative.

    Example:
        >>> convert_size(2048)
        '2.0 KB'
        >>> convert_size(1024000000)
        '976.56 MB'
    """
    if size_bytes < 0:
        raise ValueError("Size must be non-negative.")
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])
