from datetime import datetime as dt
from datetime import timedelta

REGION = "eu-central-1"
START_DATE = (dt.today() - timedelta(days=30)).isoformat(timespec="seconds")
END_DATE = dt.today().isoformat(timespec="seconds")
